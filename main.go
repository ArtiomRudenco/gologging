package main

import (
    "fmt"
    "os"
    "log"
    "io/ioutil"
    "encoding/json"
    "strconv"
    "time"
)

// Logs struct which contains an array of logs
type Logs struct {
    Logs []Log `json:"logs"`
}

// Logs struct which contains logs fields
type Log struct {
    Name   string `json:"name"`
    Type    string `json:"type"`
    Age    int    `json:"Age"`
    Social Social `json:"social"`
}

type Social struct {
    Facebook string `json:"facebook"`
    Twitter  string `json:"twitter"`
}


func main() {
    start := time.Now();
    log.Println(" ----- Start parsing logs -----")

    // Open our jsonFile
    jsonFile, err := os.Open("logs/logs.json")
    // if we os.Open returns an error then handle it
    if err != nil {
        log.Println(err)
    } else {
        log.Println(" ----- Successfully Opened logs.json -----")
    }

    // defer the closing of our jsonFile so that we can parse it later on
    defer jsonFile.Close()

    byteValue, _ := ioutil.ReadAll(jsonFile)

    var logs Logs

    json.Unmarshal(byteValue, &logs)

    for i := 0; i < len(logs.Logs); i++ {
        fmt.Println("User Type: " + logs.Logs[i].Type)
        fmt.Println("User Age: " + strconv.Itoa(logs.Logs[i].Age))
        fmt.Println("User Name: " + logs.Logs[i].Name)
        fmt.Println("Facebook Url: " + logs.Logs[i].Social.Facebook)
    }
    end := time.Now();
    log.Printf(" ----- Parsing 33k+ Users (300k+ json rows) took %v -----", end.Sub(start))


}