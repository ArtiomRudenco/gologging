package main

import(
"fmt"
"os"
)


func main() {
    // Starting
    fmt.Println("Start parsing imported logs");

    // Open our jsonFile
    jsonFile, err := os.Open("/logs/mod_info_log_customer.json")
    // if we os.Open returns an error then handle it
    if err != nil {
        fmt.Println(err)
    }

    fmt.Println("Successfully Opened logs.json")
    // defer the closing of our jsonFile so that we can parse it later on
    defer jsonFile.Close()
}
